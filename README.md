# Parnassian

Understanding and exploring the implications of the classic D.L.
Parnas paper, "On the Criteria To Be Used in Decomposing Systems
into Modules" Build two versions, one for each of the two decompositions
of the system described in the Parnas paper.  A better understanding
of how to design complex systems for "flexibility and comprehensibility"
while reducing development time.

[Parnas, D. L. (1972). On the Criteria To Be Used in Decomposing Systems into Modules. 15(12), 6.](doc/criteria_for_modularization.pdf)

## Summary

### Modularization 1 

1. Input
2. Circular Shift
3. Alphabetizing
4. Output
5. Master Control

### Modularization 2 

1. Line Storage
2. Input
3. Circular Shifter
4. Alphabetizer
5. Output
6. Master Control

  - [Getting Started](#getting-started)
  - [Running the tests](#running-the-tests)
  - [Deployment](#deployment)
  - [Built With](#built-with)
  - [Contributing](#contributing)
  - [Versioning](#versioning)
  - [Authors](#authors)
  - [License](#license)
  - [Acknowledgments](#acknowledgments)

## Getting Started

These instructions will get you a copy of the project up and running on
your local machine for development and testing purposes. See deployment
for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

    Give examples

### Installing

A step by step series of examples that tell you how to get a development
env running

Say what the step will be

    Give the example

And repeat

    until finished

End with an example of getting some data out of the system or using it
for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

    Give an example

### And coding style tests

Explain what these tests test and why

    Give an example

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

  - [Contributor Covenant](https://www.contributor-covenant.org/) - Used
    for the Code of Conduct
  - [Creative Commons](https://creativecommons.org/) - Used to choose
    the license

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions
available, see the [tags on this
repository](https://github.com/PurpleBooth/a-good-readme-template/tags).

## Authors

  - **Steven E. Newton** - *Principal Engineer* -
    [cratermoon](https://github.com/cratermoon)

## License

This project is licensed under the [CC0 1.0 Universal](LICENSE.md)
Creative Commons License - see the [LICENSE.md](LICENSE.md) file for
details

## Acknowledgments

  - Hat tip to anyone whose code was used
  - Inspiration
  - etc
