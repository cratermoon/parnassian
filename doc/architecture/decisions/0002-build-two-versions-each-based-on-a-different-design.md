# 2. Build two versions, each based on a different design

Date: 2020-11-27

## Status

Accepted

## Context

Understanding and exploring the implications of the classic D.L. Parnas paper, "On the Criteria To Be Used in Decomposing Systems into Modules"

## Decision

Build two versions, one for each of the two decompositions of the system described in the Parnas paper.

## Consequences

A better understanding of how to design complex systems for "flexibility and comprehensibility" while reducing development time
system 
