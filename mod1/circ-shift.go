package main

import (
	"unicode"
)

// wordOffset  consists of the line number for the word and the offset
// of the word within the line
type wordOffset struct {
	lineNo int
	offset int
}

// the array of all the word offset pairs after parsing
var offsets []wordOffset

func shift() {
	offsets = make([]wordOffset, 0, len(lines))
	for lineNo, line := range lines {
		inWord := false
		for j, c := range line {
			if unicode.IsLetter(c) && !inWord {
				inWord = true
				offsets = append(offsets, wordOffset{
					lineNo: lineNo,
					offset: j,
				})

			} else if !unicode.IsLetter(c) || unicode.IsPunct(c) {
				inWord = false
			}
		}
	}
}
