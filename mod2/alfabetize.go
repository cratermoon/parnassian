package main

import (
	"sort"
	"strings"
	"unicode"
)

type sortableword struct {
	word string
	pair wordOffset
}

type sortablepairs []sortableword

var sortedpairs sortablepairs

func sortPairs() {
	sortedpairs = make(sortablepairs, 0, len(offsets))
	for i := 0; i < len(offsets); i++ {
		pair := offsets[i]
		line := lines[pair.lineNo]
		wordlen := len(line) - pair.offset
		if i < (len(offsets) - 1) {
			nextPair := offsets[i+1]
			if nextPair.lineNo == pair.lineNo {
				wordlen = nextPair.offset - pair.offset - 1
			}
		}
		sortedpairs = append(sortedpairs,
			sortableword{
				word: strings.Trim(line[pair.offset : pair.offset+wordlen], " ,.\n\r"),
				pair: pair,
			})
	}
	sort.Sort(sortedpairs)
}

func (sp sortablepairs) Len() int      { return len(sp) }
func (sp sortablepairs) Swap(i, j int) { sp[i], sp[j] = sp[j], sp[i] }
func (sp sortablepairs) Less(i, j int) bool {
	ir := rune(sp[i].word[0])
	jr := rune(sp[j].word[0])
	return unicode.ToLower(ir) < unicode.ToLower(jr)
}
