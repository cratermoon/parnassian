package main

import (
	"fmt"
	"os"
)

var lines []string

func main() {
	fmt.Println("Hello, Dr. Parnassus")
	f, err := os.Open("lorem.txt")
	if err != nil {
		panic(err)
	}
	read(f)
	shift()
	sortPairs()
	write()
}
